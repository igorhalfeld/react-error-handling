import PostListContainer from './components/containers/PostListContainer';

function App() {
  return (
    <div>
      <PostListContainer />
    </div>
  );
}

export default App;
