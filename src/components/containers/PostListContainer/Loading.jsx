// can be done with: https://github.com/alexZajac/react-native-skeleton-content
export default function Loading({ children, status }) {
  if (status) {
    return <h3>loading...</h3>;
  }

  return children;
}
