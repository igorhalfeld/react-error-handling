import { useState, useEffect } from 'react';
import { getPosts } from '../../../api.js';
import { useErrorHandler, withErrorBoundary } from 'react-error-boundary';
import ErrorFallback from './ErrorFallback';
import SomeSkeletonLoading from './Loading';

function PostListContainer() {
  const [loading, setLoading] = useState(false);
  const [posts, setPosts] = useState([]);
  const handleError = useErrorHandler();

  async function fetchPosts() {
    try {
      setLoading(true);
      const data = await getPosts();
      setPosts(data);
    } catch (error) {
      handleError(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <div>
      <SomeSkeletonLoading status={loading}>
        <ul>
          {posts.map((post) => (
            <li key={post.id}>{post.title}</li>
          ))}
        </ul>
      </SomeSkeletonLoading>
    </div>
  );
}

export default withErrorBoundary(PostListContainer, {
  FallbackComponent: ErrorFallback,
  onError(error) {
    // send error to a sentry like tool
    console.log('error catched!', error);
  },
});
