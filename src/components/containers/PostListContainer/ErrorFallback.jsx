export default function PostListErrorFallback({ error, resetErrorBoundary }) {
  console.log('error', error.message);

  // it's possible to check is a axios error (like 404)
  // to show a custom ui
  return (
    <>
      <span>Some error happened: {error.message} :(</span>
      <br />
      <button onClick={resetErrorBoundary}>try again</button>
    </>
  );
}
