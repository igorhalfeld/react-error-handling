export class MyCustomError extends Error {}

const wait = (timeMs) => new Promise((resolve) => setTimeout(resolve, timeMs));

export async function getPosts() {
  const res = await fetch('https://jsonplaceholder.typicode.com/todos');
  const data = await res.json();

  const rand = Math.random();
  if (rand > 0.8) {
    throw new MyCustomError('random number is above 0.8');
  }

  // to show loading working
  await wait(2500);

  return data;
}
